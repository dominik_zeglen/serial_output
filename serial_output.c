#include "avr/io.h"
#include "avr/interrupt.h"
#include "serial_output.h"
#include "lib/bit_macros.h"

void serial_output_init()
{
	bitset(SEO_DDR, SEO_PIN);
	bitres(SEO_PORT, SEO_PIN);
	
	bitset(DDRB, 1);
	bitset(DDRB, 2);
	
	bitres(PORTB, 1);
	bitres(PORTB, 2);
	
	OCR0A = 255;
	bitset(TCCR0A, 1);			// sets WGM01 bit
	bitset(TCCR0A, 7);			// sets COM0A1 bit
	
	bitset(TCCR0B, 2);			// sets prescaler to 1024
	bitset(TCCR0B, 0);
	bitset(TIFR0, 2);			// sets OCF0A bit
	
	toggled = 0;
	interrupt_number = 0;
	serial_output_data = 0;
	
	sei();						// enable interrupts
}

void serial_output(unsigned char data)
{	
	if(toggled)
	{
		while(toggled)
		{
			;
		}
	}
	else
	{
		toggled = 1;
	} 
	
	interrupt_number = 0;
	serial_output_data = data;
	
	bitset(TIMSK0, 2);			// sets OCIE0A bit - interrupt enabled
}

ISR(TIM0_COMPA_vect)
{
	if(toggled)
	{
		bitswap(PORTB, 1);
		
		if(bitread(serial_output_data, interrupt_number))
			bitset(SEO_PORT, SEO_PIN);
		else
			bitres(SEO_PORT, SEO_PIN);
		
		interrupt_number++;
		
		if(interrupt_number == 8)
		{
			bitres(TIMSK0, 2);		// clears OCIE0A bit - interrupt disabled
			toggled = 0;
		}
	}
}
