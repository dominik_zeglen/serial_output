//	ver 1.1 
//		-defined true/false
//	ver 1.0
//		-defined basic bit operations

#ifndef __bit_macros_h_defined__
#define __bit_macros_h_defined__

#define bitset(reg, bit) (reg |= (1 << bit))
#define bitres(reg, bit) (reg &= ~(1 << bit))
#define bitread(reg, bit) (reg & (1 << bit))
#define bitswap(reg, bit) (reg ^= (1 << bit))

#define true 1
#define false 0

#endif
