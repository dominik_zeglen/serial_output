#ifndef serial_output_h_included
#define serial_output_h_included

#include "avr/io.h"

#ifndef SEO_PIN
#define SEO_PIN 0
#endif

#ifndef SEO_PORT
#define SEO_PORT PORTB
#endif

#ifndef SEO_DDR
#define SEO_DDR DDRB
#endif

static volatile unsigned char serial_output_data;
static volatile unsigned char interrupt_number;
static volatile unsigned char toggled;

void serial_output_init();
void serial_output(unsigned char);

#endif
